# Release Manager for concierge
## Installation
Install dependencies:
`npm install`
## Configuration
Copy the `.config.json` as `config.json` and configure each variable and add entries for each project:
### General variables
- `terraformsDir`: Absolute path for directory that contains subdirectories for each terraform project
- `sourcesDir`: Absolute path for directory that contains one directory for each project
### Per project variables
- `code`: Project code, must be unique
- `name`: Internal name for project 
- `tf`: Sub directory of terraform, that should exist inside the main `terraformsDir` parameter
- `src`: Sub directory of sources, that should exist inside the main `sourcesDir` parameter
- `configFile`: Relative path and file name of the config file of the project. Used to detect a commit that changed the config file
## Execution
Base execution:
`node index`
### Arguments
- `--no-pull`: Avoids the `git pull` of remotes on tf and src projects.
- `--no-pull-src`: Avoids the `git pull` of sources projects only
- `--no-pull-tf`: Avoids the `git pull` of terraform projects only