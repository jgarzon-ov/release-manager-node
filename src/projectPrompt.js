import prompts from 'prompts';
import chalk from 'chalk';
import config from './config.js';
import args from './args.js'

async function promptProjects() {
  const choices = config.projects.map(p => ({ title: p.name, value: p, selected: true }))
  const selection = await prompts({
    type: 'multiselect',
    instructions: false,
    min: 1,
    name: 'projects',
    message: 'Which projects to scan and verify?',
    choices
  })
  return selection.projects
}

async function confirmProjects(projects) {
  console.log()
  console.log(projects.length, "project/s have been selected:")
  for (let p of projects) {
    console.log(chalk.greenBright(` - ${p.name}`))
  }
  if (args.pull && (args.pullTf || args.pullSrc)) {
    let projMsg = ""
    if (args.pullTf && args.pullSrc) projMsg = "sources and terraform "
    else if (args.pullSrc) projMsg = "sources "
    else if (args.pullTf) projMsg = "terraforms "
    console.log(chalk.redBright(`The ${projMsg}projects will updated and refreshed with remotes (git pull --all) `))

  }
  const resp = await prompts({
    type: 'confirm',
    name: "value",
    message: 'Can you confirm?',
    initial: false
  })
  return resp.value
}

export default { promptProjects, confirmProjects }
