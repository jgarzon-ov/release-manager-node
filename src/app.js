import projectPrompt from './projectPrompt.js'
import { diagnoseProject } from './diagnose.js'
import { showConfirmationTable } from './confirmation.js'
import { processProject } from './process.js'
import prompts from 'prompts';
import args from './args.js'

async function generateChangeset() {
  const projects = await projectPrompt.promptProjects()
  var executionData = {}
  if (!await projectPrompt.confirmProjects(projects)) {
    return
  }
  for (var project of projects) {
    const version = await prompts({
      type: 'text',
      name: "value",
      message: 'From version for changeset'
    })
    project.forcedLastVersion = version.value
    await diagnoseProject(project, executionData, true)
  }
  for (var projCode of projects) {
    let project = executionData[projCode.code]
    console.log(`h2. ${projCode.name}`)
    console.log('h5. Changeset:')
    for (var commit of project.commitLogs) {
      if (!commit.subject.startsWith('Merge ') && !commit.subject.startsWith('Merged ')) {
        console.log("- ", commit.subject ? commit.subject.trim() : " ")
      }
      console.log("")
    }
  }
}

async function generateReleases() {
  const projects = await projectPrompt.promptProjects()
  var executionData = {}
  if (!await projectPrompt.confirmProjects(projects)) {
    return
  }
  for (var project of projects) {
    await diagnoseProject(project, executionData)
  }

  const releases = await showConfirmationTable(executionData)
  if (!releases) {
    console.log("Nothing to do, exiting")
    process.exit(0)
  }

  for (var projCode of releases) {
    try {

      await processProject(executionData[projCode])
    } catch {
      const resp = await prompts({
        type: 'confirm',
        name: "value",
        message: 'The project finished with errors... continue with next project?',
        initial: false
      })
      if (!resp.value) {
        return
      }
    }
  }
  console.log("SUMMARY")
  for (var projCode of releases) {
    let project = executionData[projCode]
    console.log(`h2. ${projCode} (${project.sourceNewVersion.name}) > TF: ${project.terraformNewVersion.name}`)
    console.log('h5. Config changes:')
    if (!project.configChangeCommit) {

      console.log(' (no config changes)')
    } else {
      console.log(' [COMPLETE]')
    }
    console.log('h5. Changeset:')
    for (var commit of project.commitLogs) {
      if (!commit.subject.startsWith('Merge ') && !commit.subject.startsWith('Merged ')) {
        console.log("- ", commit.subject ? commit.subject.trim() : " ")
      }
      console.log("")
    }
  }
}
async function run() {
  if (args.onlyChangeset) {
    return generateChangeset()
  } else {
    return generateReleases()
  }

}

export default run;