import Table from 'tty-table'
import chalk from 'chalk';
import prompts from 'prompts';

async function showConfirmationTable(executionMeta) {
  let headers = []
  headers.push({
    value: "name",
    alias: "Project",
    color: "white",
    align: "left"
  })
  headers.push({
    value: "codeChanged",
    alias: "Code changed?",
    color: "white",
    align: "left"
  })
  headers.push({
    value: "configChanged",
    alias: "Config changed?",
    color: "white",
    align: "left"
  })
  headers.push({
    value: "currentVersion",
    alias: "Current version",
    color: "white",
    align: "left"
  })
  headers.push({
    value: "nextVersion",
    alias: "Version to release",
    color: "white",
    align: "left"
  })

  let rows = []
  const yes = chalk.green('YES')
  const no = chalk.red('NO')
  let projectsToRelease = []
  for (let projCode in executionMeta) {
    let proj = executionMeta[projCode]
    let row = {}
    row.name = proj.name
    if (proj.modifiedFileCount > 0 || proj.force) {
      row.codeChanged = yes + ' ' + chalk.gray(proj.modifiedFileCount + ' files changed')
      row.nextVersion = 'src: ' + chalk.greenBright(proj.sourceNewVersion.name) + '\n' + 'tf: ' + chalk.greenBright(proj.terraformNewVersion.name)
      projectsToRelease.push({
        title: chalk.yellow(proj.name) + ' ' + chalk.dim(`source version: ${proj.sourceNewVersion.name}, terraform version: ${proj.terraformNewVersion.name}`),
        value: projCode, selected: true
      })
      if (proj.linkedProject) {
        row.nextVersion += '\n(Source linked from ' + chalk.yellowBright(proj.linkedProject.name) + ')'
      }
    } else {
      row.codeChanged = no + chalk.redBright(' [won\'t release!]')
      row.nextVersion = chalk.redBright('won\'t release')
    }
    let configString = ''
    if (proj.configChangeCommit) {
      for (var item of proj.configChangeCommit.items) {
        configString += chalk.gray(item.subject.substr(0,15)) + chalk.blueBright(' [' + item.authorName + ']\n')
      }
    } else {
      configString = no
    }
    row.configChanged = configString
    row.currentVersion = 'src: ' + chalk.greenBright(proj.sourceLastVersion?.name) + '\n' + 'tf: ' + chalk.greenBright(proj.terraformLastVersion.name)
    rows.push(row)
  }
  const options = {
    borderStyle: "solid",
    borderColor: "white",
    headerAlign: "left",
    align: "left",
    color: "white",
    width: "90%"
  }
  const out = Table(headers, rows, options).render()
  console.log(out);
  console.log()
  const selection = await prompts({
    type: 'multiselect',
    instructions: false,
    min: 1,
    name: 'projects',
    message: 'The following projects will be released, please select them to config',
    choices: projectsToRelease
  })
  return selection.projects

}
export { showConfirmationTable }