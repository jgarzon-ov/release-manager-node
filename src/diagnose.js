import chalk from 'chalk';
import { execprocess, gitCommand } from './utils.js'
import bitbucket from 'bitbucket'
import config from './config.js';
import path from 'path'
import args from './args.js'
import { gitlogPromise } from "gitlog";
import { exit } from 'process';
const versionRegex = /^(\d*)\.(\d*)\.(\d*)$/
const tagRegex = /tag\:\s*(.*)/
async function lastVersion(path) {
  try {
    const out = await execprocess('git tag | cat | sort -Vr | head', { cwd: path })
    let lines = out.stdout.split(/\r\n|\r|\n/)
    let found = null
    let i = 0
    while (!found && i < lines.length) {
      let match = lines[i].match(versionRegex)
      if (match) {
        found = { major: parseInt(match[1]), minor: parseInt(match[2]), patch: parseInt(match[3]), name: match[0] }
      }
      i++
    }
    return found

  } catch (e) {
    console.error("Error executing command " + chalk.inverse(e))
  }
}
async function processPendingPRs(project) {
  if (config.atlassian && project.repo) {

    const clientOptions = {
      auth: config.atlassian.auth
    }
    const bitbucketConnection = new bitbucket.Bitbucket(clientOptions)
    const resp = await bitbucketConnection.pullrequests.list({
      repo_slug: project.repo,
      workspace: config.atlassian.workspace,
      state: 'OPEN'
    })
    console.log("Atlassian resp", resp)
  } else {
    console.log('No bitbucket config found')
  }
}
function parseTag(tagString) {
  const parts = tagString.split(',')
  for (let part of parts) {
    const match = part.match(tagRegex)
    if (match) {
      return match[1]
    }
  }
  return null
}
async function processCommitLogs(project, path, projectMeta, forChangeset) {
  let count = forChangeset ? 50 : 20
  var logs = await gitlogPromise({
    repo: path,
    number: count,
    includeMergeCommitFiles: false,
    fields: ["hash", "subject", "tag", 'authorName', 'authorDate', 'rawBody']
  })
  var logsWithMerge = await gitlogPromise({
    repo: path,
    number: count,
    includeMergeCommitFiles: true,
    fields: ["hash", "subject", "tag", 'authorName', 'authorDate']
  })
  //remove duplicates
  var allLogs = [...logs]
  for (var logItem of logsWithMerge) {
    if (!allLogs.find(k => k.hash === logItem.hash)) {
      allLogs.push({ ...logItem, merge: true })
    } else {
    }
  }
  let foundVersionTag = false
  let i = 0
  let filteredLogs = []
  let modifiedFileCount = 0
  while (!foundVersionTag && i < allLogs.length) {
    const logItem = allLogs[i]
    const tag = parseTag(logItem.tag)
    if (tag === projectMeta.sourceLastVersion?.name && !logItem.merge) {
      foundVersionTag = true
    } else {

      if (logItem.files && project.configFile) {
        const foundConfigChange = logItem.files.find(f => f === project.configFile)
        if (foundConfigChange) {
          if (projectMeta.configChangeCommit) {
            if (!projectMeta.configChangeCommit.items.find(i => i.hash === logItem.hash))
              projectMeta.configChangeCommit.items.push(logItem)
          } else {
            projectMeta.configChangeCommit = {
              items: [logItem]
            }
          }

        }
      }
      if (!logItem.merge && !foundVersionTag) {

        modifiedFileCount += logItem.files.length
        filteredLogs.push(logItem)
      }
    }
    i++;
  }
  projectMeta.modifiedFileCount = modifiedFileCount
  projectMeta.commitLogs = filteredLogs
  if (args.force) {
    const parts = args.force.split(',')
    console.log("FORCING", parts, projectMeta.code)
    if (parts.length && parts.find(f=>f===projectMeta.code)) {
      projectMeta.force = true
    }
  }

}
function nextVersion(v, type) {
  let newV = Object.assign({}, v)
  if (args['version-' + type]) {
    newV.name = args['version-' + type]
    console.log(" Forcing version " + newV.name)
    return newV
  }
  const verInc = args['version-increment-' + type]
  if (verInc === 'patch') {
    newV.patch++
  } else if (verInc === 'minor') {
    newV.minor++
    newV.patch = 0
  } else if (verInc === 'major') {
    newV.major++
    newV.patch = 0
    newV.minor = 0
  }

  newV.name = newV.major + '.' + newV.minor + '.' + newV.patch
  return newV
}
async function diagnoseProject(project, executionMeta, forChangeset = false) {
  console.log(chalk.underline('Diagnosing ', chalk.bold.greenBright(project.name)));
  const srcDir = path.join(config.sourcesDir, project.src)
  const tfDir = path.join(config.terraformsDir, project.tf)
  const processSource = !project.linkSource || project.linkSource === ""
  let linkedProject = null
  if (!processSource) {
    linkedProject = executionMeta[project.linkSource]
  }
  if (!processSource && !linkedProject) {
    console.log(chalk.red('Linked project ' + project.linkSource + ' must be processed before this one'))
    exit(1)
  }
  let currentBranchOut
  if (processSource) {

    currentBranchOut = await gitCommand('branch --show-current', srcDir, ' detecting current branch in source')
    if (currentBranchOut.stdout.trim() !== 'develop') {
      console.log(chalk.red('Project not in develop branch... ' + currentBranchOut.stdout.trim()))
      exit(1)
    }
  } else {
    console.log("Skipping source check for this project")
  }
  const currentTfBranchOut = await gitCommand('branch --show-current', tfDir, ' detecting current branch in terraform')
  if (currentTfBranchOut.stdout.trim() !== 'develop') {
    console.log(chalk.red('Project not in develop branch... ' + currentTfBranchOut.stdout.trim()))
    exit(1)
  }
  if (args.pull && args.pullSrc && processSource)
    await gitCommand('pull --all', srcDir, ' pulling source code')
  if (args.pull && args.pullTf)
    await gitCommand('pull --all', tfDir, ' pulling terraform project')
  var projectMeta = executionMeta[project.code]
  if (!projectMeta) {
    projectMeta = {}
    executionMeta[project.code] = projectMeta
  }
  projectMeta.name = project.name
  projectMeta.code = project.code
  if (processSource) {
    projectMeta.currentBranch = currentBranchOut.stdout.trim()
    if (project.forcedLastVersion) {
      projectMeta.sourceLastVersion = project.forcedLastVersion
    } else {
      projectMeta.sourceLastVersion = await lastVersion(srcDir)
    }
    projectMeta.sourceNewVersion = nextVersion(projectMeta.sourceLastVersion, 'src')
  } else {

    projectMeta.currentBranch = linkedProject.currentBranch
    projectMeta.sourceLastVersion = linkedProject.sourceLastVersion
    projectMeta.sourceNewVersion = linkedProject.sourceNewVersion
    projectMeta.linkedProject = linkedProject
    console.log(" > Project linked from", chalk.yellowBright(projectMeta.linkedProject.name) + '!')
  }
  projectMeta.srcDir = srcDir
  projectMeta.tfDir = tfDir
  projectMeta.terraformLastVersion = await lastVersion(tfDir)
  projectMeta.terraformNewVersion = nextVersion(projectMeta.terraformLastVersion, 'tf')
  await processCommitLogs(project, srcDir, projectMeta, forChangeset)
  if (projectMeta.configChangeCommit) {
    for (var item of projectMeta.configChangeCommit.items) {
      const configChangeDiff = await gitCommand('show --oneline --unified=0 ' + item.hash + ' --  ' + project.configFile, srcDir, ' retrieving config diff')
      item.details = configChangeDiff.stdout
    }
  }
  //await processPendingPRs(project)
}

export { diagnoseProject }