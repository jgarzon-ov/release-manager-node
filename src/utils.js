import { exec, spawn } from 'child_process'
import chalk from 'chalk';

function execprocess(cmd, opts) {
  opts || (opts = {});
  return new Promise((resolve, reject) => {
    const child = exec(cmd, opts,
      (err, stdout, stderr) => err ? reject({ cmd, err }) : resolve({
        stdout: stdout,
        stderr: stderr
      }));

    if (opts.stdout) {
      child.stdout.pipe(opts.stdout);
    }
    if (opts.stderr) {
      child.stderr.pipe(opts.stderr);
    }
  });
}
function pipeProcess(cmd, path) {
  return new Promise((resolve, reject) => {
    let output = '';
    const process = spawn('sh', ['-c', args], { cwd: path });
    process.stdout.on('data', (chunk) => {
      output += chunk.toString();
    });

    process.on('close', function (code) {
      resolve({ code, stdout: process.stdout, stderr: process.stderr, output });
    });
    process.on('error', function (err) {
      reject({ err, stdout: process.stdout, stderr: process.stderr, output });
    });
  });
}
async function gitCommand(command, path, msg, env) {
  if (msg) process.stdout.write(msg + " " + chalk.dim(path))
  try {
    const out = await execprocess(( env || '') + 'git ' + command, { cwd: path })
    if (msg) process.stdout.write(chalk.green(" OK\n"))
    return out
  } catch (e) {
    console.error(chalk.red(" Error executing command ") + chalk.inverse(e.cmd) + ' ' + chalk.redBright(e.err))
    throw e
  }
}

export {
  execprocess,
  pipeProcess,
  gitCommand
}