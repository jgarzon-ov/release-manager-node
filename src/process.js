
import { execprocess, gitCommand } from './utils.js'
import path from 'path'
import fs from 'fs'
import prompts from 'prompts';

import chalk from 'chalk';
const regExpTFVersion = /(\s*image_version\s*\=\s*\")(.*?)(\".*?)/
async function processPackageJson(projectMeta, filename) {
  var file_content = fs.readFileSync(filename);
  var content = JSON.parse(file_content);
  content.version = projectMeta.sourceNewVersion.name
  process.stdout.write("> Changing version in " + chalk.greenBright('package.json') + " to " + chalk.greenBright(projectMeta.sourceNewVersion.name))
  fs.writeFileSync(filename, JSON.stringify(content, null, 2));
  process.stdout.write(chalk.green(" OK\n"))
  process.stdout.write("> executing npm install in order to update " + chalk.greenBright('package-lock.json'))
  await execprocess('npm install --legacy-peer-deps', { cwd: projectMeta.srcDir })
  process.stdout.write(chalk.green(" OK\n"))
  await gitCommand('commit -m "version bump" package.json package-lock.json', projectMeta.srcDir, '> Bumping package.json and package-lock.json versions')
}
async function finishSourceRelease(projectMeta) {
  await gitCommand(`checkout master`, projectMeta.srcDir, `Checkout master branch for ${projectMeta.srcDir}`)
  await gitCommand(`pull`, projectMeta.srcDir, `Pulling master branch for ${projectMeta.srcDir}`)
  await gitCommand(`flow release finish -m "release" ${projectMeta.sourceNewVersion.name}`, projectMeta.srcDir, `Finishing release ${projectMeta.sourceNewVersion.name}`, 'GIT_MERGE_AUTOEDIT=no ')
  //console.log(`flow release finish ${projectMeta.sourceNewVersion.name}`, projectMeta.srcDir, `Finishing release ${projectMeta.sourceNewVersion.name}`)
}
async function generateSourceRelease(projectMeta) {
  await gitCommand(`flow release start ${projectMeta.sourceNewVersion.name}`, projectMeta.srcDir, `Starting release ${projectMeta.sourceNewVersion.name}`)
  //console.log(`flow release start ${projectMeta.sourceNewVersion.name}`, projectMeta.srcDir, `Starting release ${projectMeta.sourceNewVersion.name}`)
  const packageJson = path.join(projectMeta.srcDir, 'package.json')
  if (fs.existsSync(packageJson)) {
    await processPackageJson(projectMeta, packageJson)
  }
}
async function generateTerraformRelease(projectMeta) {
  await gitCommand(`flow release start ${projectMeta.terraformNewVersion.name}`, projectMeta.tfDir, `Starting release ${projectMeta.terraformNewVersion.name}`)
  await gitCommand(`checkout master`, projectMeta.tfDir, `Checkout master branch for ${projectMeta.tfDir}`)
  await gitCommand(`pull`, projectMeta.tfDir, `Pulling master branch for ${projectMeta.tfDir}`)
  await gitCommand(`flow release finish -m "release" ${projectMeta.terraformNewVersion.name}`, projectMeta.tfDir, `Finishing release ${projectMeta.terraformNewVersion.name}`,  'GIT_MERGE_AUTOEDIT=no ')
  //console.log(`flow release finish ${projectMeta.terraformNewVersion.name}`, projectMeta.tfDir, `Finishing release ${projectMeta.terraformNewVersion.name}`)
}
async function pushTerraform(projectMeta) {
  await gitCommand(`push`, projectMeta.tfDir, `Pushing terraform develop branch`)
  await gitCommand(`push origin master`, projectMeta.tfDir, `Pushing terraform master branch`)
  await gitCommand(`push --tags`, projectMeta.tfDir, `Pushing terraform tags`)
  console.log(`push`, projectMeta.tfDir, `Pushing terraform develop branch`)
  console.log(`push origin master`, projectMeta.tfDir, `Pushing terraform master branch`)
  console.log(`push --tags`, projectMeta.tfDir, `Pushing terraform tags`)
}
async function pushSource(projectMeta) {
  await gitCommand(`push`, projectMeta.srcDir, `Pushing source code`)
  await gitCommand(`push origin master`, projectMeta.srcDir, `Pushing master`)
  await gitCommand(`push --tags`, projectMeta.srcDir, `Pushing tags`)
  //console.log(`push`, projectMeta.srcDir, `Pushing develop branch`)
  //console.log(`push origin master`, projectMeta.srcDir, `Pushing master branch`)
  //console.log(`push --tags`, projectMeta.srcDir, `Pushing tags`)
}
async function updateTerraform(projectMeta) {
  const mainTF = path.join(projectMeta.tfDir, 'main.tf')
  if (projectMeta.configChangeCommit) {
    process.stdout.write("> Config changes have been detected with the following information: \n")
    let count = 1
    for (let item of projectMeta.configChangeCommit.items) {
      process.stdout.write(chalk.yellow(`Config change #${count++}`) + '\n')
      process.stdout.write(chalk.dim(item.details) + '\n')
    }
    process.stdout.write(chalk.yellow('Please edit manually the terraform config files and confirm when its done. Also commit the change!'))
    let resp = { value: false }
    let retry =3
    while (!resp.value && retry-->0) {
      resp = await prompts({
        type: 'confirm',
        name: "value",
        message: 'Did you commited the config change in the terraform project?',
        initial: false
      })
    }
    if (!resp.value) {
      return false
    }
  }
  process.stdout.write("> Changing image_version in " + chalk.greenBright('main.tf') + " to " + chalk.greenBright(projectMeta.sourceNewVersion.name))
  var fileContent = fs.readFileSync(mainTF);
  var updatedFile = fileContent.toString().replace(regExpTFVersion, "$1" + projectMeta.sourceNewVersion.name + "$3")
  fs.writeFileSync(mainTF, updatedFile);
  process.stdout.write(chalk.green(" OK\n"))
  await gitCommand('commit -m "version upgrade" main.tf', projectMeta.tfDir, '> Upgrading version in main.tf')

  return true
}

async function processProject(projectMeta) {
  console.log()
  console.log(chalk.yellow.underline.bold("Processing ", projectMeta.name))
  console.log()
  if (!projectMeta.linkedProject) {

    await generateSourceRelease(projectMeta)
    await finishSourceRelease(projectMeta)
    await pushSource(projectMeta)
  } else {
    console.log( ' Skipping source release generation, using previously generated ' + projectMeta.linkedProject.sourceNewVersion.name)
  }
  let tfUpdate = await updateTerraform(projectMeta)
  if (tfUpdate) {
    await generateTerraformRelease(projectMeta)
    await await pushTerraform(projectMeta)
  }
}
export {
  processProject
}