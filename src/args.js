import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

export default yargs(hideBin(process.argv))
  .describe('no-pull', 'Avoids the `git pull` of remotes on tf and src projects.')
  .describe('no-pull-src', 'Avoids the `git pull` of sources projects only.')
  .describe('no-pull-tf', 'Avoids the `git pull` of terraforms projects only.')
  .describe('version-increment-src', 'Type of version increment for source projects (major, minor, patch. Default: patch)')
  .describe('version-increment-tf', 'Type of version increment for terraform projects (major, minor, patch. Default: patch)')
  .describe('version-src', 'force version for source')
  .describe('version-tf', 'force version for tf')
  .describe('force', 'Comma-separated list of projects to include even if no code changes detected')
  .describe('only-changeset', 'Only generate changeset')
  .default('pull', true)
  .default('pull-src', true)
  .default('pull-tf', true)
  .default('version-increment-src', 'patch')
  .default('version-increment-tf', 'patch')
  .default('force','')
  .argv

  