import fs from 'fs'
var config
try {

  const data =  fs.readFileSync(new URL('../config.json', import.meta.url), 'utf8');
  config = JSON.parse(data)
}catch (err) {
  console.error('No config file', err)
}

export default config;